import { RouterModule, Routes } from '@angular/router';

import { PostComponent } from './post/post.component';
import { PostsComponent } from './posts/posts.component';

const app_routes: Routes = [
    { path: 'post', component: PostComponent},
    { path: 'posts/:id', component: PostsComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'post'}
];



export const app_routing = RouterModule.forRoot(app_routes);